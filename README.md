# README #

Vežba za predmet Veštačka Inteligencija, Fakultet za Računarstvo Informatiku na Univerzitetu Singidunum, rađeno 2016. godine.

Jednostavna Puzzle Eight mini-igra, sa izračunanjem najmanjeg broja poteza za rešavanje problema.

Aplikacija koristi A* algoritam pretrage sa heurističkom funkcijom Manhattan distance.

Primer aplikacije: http://198.46.157.26/